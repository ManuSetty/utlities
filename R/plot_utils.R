# Set of utility functions for plotting

# Revised         Comments
# 11/23/2009     Added subtypes parameter to heatmap function
#                 Moved the plot.ecdf function to this file
# 12/07/2009     Updated limits option to min(x), max(x) in plot.ecdf
# 12/18/2009     Added title parameter to plot lines
# 12/20/2009     Axis labels to plot lines
# 01/19/2010     Added legend to heatmap
# 02/08/2010     Added optional argument to heatmap.2.custom
# 04/29/2010     Added plot.campare and add.to.ecdf functions
# 04/30/2010     Updated plot.compare to plot in different colors
# 06/24/2010     Removed reference to heatmap.22
# 01/19/2011     Added dendro.plot function
# 02/04/2011     Added plot.pca, plot.km function
# 02/17/2011     Revised plot.compare,plot.lines with ggplot2, Added barplot.custom
# 03/08/2011     barplot.custom maintains order of input settings
# 01/09/2011     Updated cdf functions to use stepfunction
# 02/16/2012     dendro.plot function can now measure distance by correlation.
# 02/28/2012     Updated colors.list from colorbrewer

#' @export
colors.list <- c('#E41A1C', '#377EB8' ,'#4DAF4A', '#984EA3', '#FF7F00', '#FFFF33', '#A65628',
                 '#8DD3C7', '#BEBADA', '#FB8072', '#FDB462')

# Heatmap.2 function
#' @export
heatmap.2.custom <- function (matrix, scale="none", trace="none", col=greenred(25),  subtypes=NA, dendro="col", ...){

  require (gplots)

  if (hasArg (subtypes)) {
    if (length (subtypes) != ncol (matrix))
      stop ('Length of subtypes not equal to no of cols in matrix')
  }
  
  ColSideColors <- rep (colors.list[1], ncol(matrix))
  # Update colors if subtypes is specified
  if (hasArg(subtypes)) {
    unique.types <- sort (unique (subtypes))
    names (colors.list) <- unique.types
    ColSideColors <- colors.list[subtypes]
  }
  
  heatmap.res <- heatmap.2 (matrix, scale=scale, trace=trace, col=col,  ColSideColors = ColSideColors, dendro=dendro, symbreaks=TRUE, ...)
  if (hasArg (subtypes))
    legend ('bottomleft', as.vector (unique.types), cex=0.7, fill=colors.list[1:length (unique.types)], bty='n')

  invisible (heatmap.res)
}

# Plot multiple lines in the same graph
#' @export
plot.lines <- function (plot.data, title="", xlab="", ylab="") {

  lengths <- sapply (plot.data, length)
  df <- data.frame (x=unlist (lapply (lengths, function (x) {1:x})),
                    y=unlist (plot.data),
                    Class=rep (labels (plot.data), times=lengths))
  p <- ggplot (df, aes (x, y, col=Class))
  p <- p + geom_line ()
  p <- p + xlab (xlab) + ylab (ylab) + opts(title=title)
  print (p)
}

# x,y :      Vectors for which the ecdfs have to be compared. y is assumed to be the background the p-values calculated for over and under expression of x as compared to y
# legends:   Vector of two elements representing the set of x and y

#' @export
plot.ecdf <- function (x ,y, xlim=c(min(x), max(x)), title='ECDF', xlab='Values', ylab='Fraction', legends=NA) {

  p.value.over <- ks.test (x, y, alternative="less")$p.value
  p.value.under <- ks.test (x, y, alternative="greater")$p.value
  
  ylim = c(0, 1)
  plot (0, 0, xlim=xlim, ylim=ylim, type="n", xlab=xlab, ylab=ylab, main=title)
  plot (stepfun (sort (x), (0:length(x))/length(x)), col="red", add=TRUE, pch='')
  plot (stepfun (sort (y), (0:length(y))/length(y)), col="black", add=TRUE, pch='')
  
  if (hasArg(legends))
    legend ("bottomright", legend=legends, col=c("red", "black"), lty=1, bty="n")
  legend ("topleft", legend=sprintf ("Over: %1.2e\nUnder: %1.2e", p.value.over, p.value.under), lty=0, bty="n")
  
}

# Plot a rectangular grid of values with rainbow colours
#' @export
plot.grid <- function (matrix) {
  no.colors <- length (unique (as.vector (unlist (matrix))))
  
  image(t(matrix), axes = FALSE, xlab = "", ylab = "", col=greenred(no.colors))
}


#' @export
plot.compare <- function (x, y, xlab='x', ylab='y', title='',subtypes='') {

  df <- data.frame (x, y, Subtype=subtypes)

  p <- ggplot (df, aes (x, y, col=Subtype))
  p <- p + geom_point () 
  p <- p + geom_abline (col='black', linetype=2, size=0.3) 
  p <- p + labs (x=xlab, y=ylab) + opts (title=title)
  p <- p + xlim(range (x,y)) + ylim (range (x,y))
  p <- p + theme_bw()
  print (p)
}


#' @export
add.to.ecdf <- function (data, col="blue", ...) {
  plot (stepfun (sort (data), (0:length (data))/length (data)), col=col, add=TRUE, pch='', ...)
}
  


# Plot dendrogram
#' @export
dendro.plot <- function (data, subtypes, nodePar=list(), dendro.type='Dist', lab.cex=0.5, ...) {


  if (hasArg (subtypes)) {
    if (length (subtypes) != ncol (data))
      stop ('Length of subtypes not equal to no of cols in data')
  }
  
  colLab <- function(n) {
    if(is.leaf(n)) {
      a <- attributes(n)
      attr(n, "nodePar") <-
        c(a$nodePar, nodePar, lab.cex=lab.cex,
          list(col = colors[a$label], pch=20, cex=2))
    }
    n
  }

  ## Determine dendrogram
  if (dendro.type == 'Dist') {
    dendro <- as.dendrogram (hclust (dist (t (data))))
  } else if (dendro.type == 'Cor') {
    dendro <- as.dendrogram (hclust (as.dist (1-cor (data, method='s'))))
  } else {
    stop ('dendro.type can either be Dist or Cor')
  }
  
  ## Set column names to be subtype names
  if (hasArg  (subtypes)) {
    names (colors.list) <- sort (unique (subtypes))
    colors <- colors.list[subtypes]
    names (colors) <- colnames (data)
    dendro <- dendrapply (dendro, colLab)
  }

  plot (dendro, yaxt='n', ...)
  if (hasArg (subtypes)) {
    l <- names (colors.list)[!is.na (colors.list)]
    legend ('topright', legend=l, text.col=colors.list[l], bty='n',cex=0.75 )
  }

  invisible (dendro)
  
}



## PCA and plot first three components
#' @export
plot.pca <- function (data, subtypes, title='',...) {
  pca.res <- princomp (data, scores=TRUE)
  components <- as.matrix (loadings (pca.res))
  
  par (mfrow=c(2,2))
  names (colors.list) <- unique (subtypes)
  plot (components[,1], components[,2], xlim=range (components[,1]), ylim=range (components[,2]), pch=19,
        xlab='PC1', ylab='PC2', main='', col=colors.list[subtypes], ...)
  plot (components[,2], components[,3], xlim=range (components[,2]), ylim=range (components[,3]), pch=19,
        xlab='PC2', ylab='PC3', main='', col=colors.list[subtypes], ...)
  plot (components[,1], components[,3], xlim=range (components[,1]), ylim=range (components[,3]), pch=19,
        xlab='PC1', ylab='PC3', main='', col=colors.list[subtypes], ...)
  plot.new ()

  legend ('topleft', legend=title, bty='n', cex=1.5)
  legend ('bottomright', legend=names (colors.list), bty='n', text.col=colors.list, cex=1.25)
  par (mfrow=c(1,1))
  
  invisible (pca.res)
}


## Kaplan-Meier plot
#' @export
plot.km <- function (time, status, classes, title='', ...) {
 
  plot.data <- data.frame (time=time, status=status, x=classes)
  plot.data <- plot.data[sort (plot.data$x, index.return=TRUE)$ix,]

  unique.classes <- unique (plot.data$x)
  names (colors.list) <- unique.classes
  ## Fit and plot survival
  fit <- survfit (Surv(time, status) ~ x, data = plot.data)
  plot (fit, lwd=2, col=colors.list[1:length (unique.classes)], ...)
  ## Median survivals
  m <- round (tapply (plot.data$time, plot.data$x, median), 2)
  legend ('topright', legend=paste (unique.classes, m[unique.classes], sep=': '),
          text.col=colors.list[1:length (unique.classes)], bty='n')
  
  ## Determine p-value
  diff <- survdiff (Surv(time, status) ~ x, data = plot.data)
  pval <- pchisq (diff$chisq, length (diff$n)-1, lower.tail=FALSE)
  title (sprintf ('%s: %.2e', title, pval))

  return (list (pval=pval, medians=m))
}


## Custom barplot
#' @export
barplot.custom <- function (plot.data, subtypes='', label='Y', title='') {

  dodge <- position_dodge (width=0.9)
  ## Create a factor of Setting to maintain the order
  Setting <- rep (labels (plot.data), each=sapply (plot.data, length))
  Setting <- factor (Setting, levels=labels (plot.data))
  df <- data.frame (Y=unlist (plot.data), Setting=Setting,
                    Subtypes = rep (subtypes, length (plot.data)))
  
  ## If subtypes are specified, replicate the entire data frame and create a combined entry
  if (hasArg (subtypes)) {
    nrows <- nrow (df)
    df <- rbind (df, df)
    df[1:nrows, 'Subtypes'] <- rep ('All', nrows)
  }

  p <- ggplot (df, aes (x=Subtypes, y=Y, fill=Setting))
  p <- p + geom_bar (position=dodge, stat='summary', fun.y='mean')
  p <- p + geom_errorbar (fun.data=mean_cl_normal, width=0.25, stat='summary', position=dodge)
  p <- p + ylab (label) + opts (title=title)
  p <- p + theme_bw ()
  print (p)
}


## Function for plotting densities on the same plot
#' @export
plot.densities <- function (events, colors=colors.list, scale=TRUE, ...) {

  ## Compute densities
  densities <- sapply (events, function (x) { if (length (x) < 2) return (NULL);
                                              density (x, na.rm=TRUE)}, simplify=FALSE)
  names (densities) <- sprintf ("%s-%d", names (events), sapply (events, length))
  if (scale)
    densities <- sapply (densities, function (x)
                         { if (is.null (x$y)) return (NULL)
                           x$y <- x$y/max (x$y); return (x)}, simplify=FALSE)
                         
  ## Limits to span all densities
  xlim <- range (unlist (lapply (densities, '[[', 'x')))
  ylim <- range (unlist (lapply (densities, '[[', 'y')))

  ## Plot densities
  plot (0, 0, type='n', xlim=xlim, ylim=ylim, ...)
  par (lwd=2)
  for (i in 1:length (densities)) {
    if (is.null (densities[[i]]))
      next
    lines (densities[[i]]$x, densities[[i]]$y, col=colors[i])
  }
  legend ('topleft', legend=names (densities),
         col=colors[1:length (events)], lty=1, bty='n')
}
  
  

