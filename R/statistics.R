# Set of functions related to statistics
# Manu N Setty
# 11/30/2009

# Revised         Comments
# 07/01/2011      Added ks.test.custom functiong


# Function to sample from a vector with priors
# x            Vector of values. Sampling will be from this set
# priors     Prior for each value in x. Uniform prior is assumed if this is not
#               specified
# n            Number of times sampling should be done.
#' @export
sampling.with.priors <- function (x, n, priors) {

  count <- length (x)
  if (!hasArg (priors))
    priors <- rep (1/count, count)
  
  if (count != length (priors))
    stop ("x and priors are of different length")

  if (sum (priors) != 1)
    stop ("priors do not sum to 1")
  
  cdf <- cumsum (priors)
  sampling <- runif (n)
  sampling <- as.vector (sapply (sampling, function (x) { which (x <= cdf)[1] }))
  sampling <- x[sampling]

  return (sampling)
}

                                 
#' @export
ks.test.custom <- function (x, y, ...) {

  ks.res <- ks.test (x, y, ...)

  x <- sort (x)
  y <- sort (y)
  c.x <- cumsum (rep (1/length (x), length (x)))
  c.y <- cumsum (rep (1/length (y), length (y)))
  names (c.y) <- y
  diffs <- c.x - c.y[as.character (x)]
  ks.res$statistic.at <- x[abs (diffs) == max (abs (diffs))]
  if (length (grep ('lies below', ks.res$alternative)) > 0)
    ks.res$statistic.at <- x[diffs == min (diffs)]
  if (length (grep ('lies above', ks.res$alternative)) > 0)
    ks.res$statistic.at <- x[diffs == max (diffs)]
  
  return (ks.res)
}
