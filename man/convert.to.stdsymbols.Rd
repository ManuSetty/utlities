\name{convert.to.stdsymbols}
\alias{convert.to.stdsymbols}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Gene aliases to symbols
}
\description{
Function to convert gene aliases to standard symbols. This function uses the org.Hs.eg.db package for the conversion. 
The aliases are first converted to entrez identifiers. The gene symbols are then obtained from the entrez identifiers. The list of gene aliases should be unique. This assumes that the package org.Hs.eg.db is installed.
}
\usage{
convert.to.stdsymbols(genes.aliases)
}

\arguments{
  \item{genes.aliases}{
A vector of unique gene aliases
}
}
\details{

}
\value{
Named vector of gene symbols
}
\references{

}
\author{

}
\note{

}


\section{Warning}{
The BioConductor package org.Hs.eg.db must be installed for this function to work
}

\seealso{

}
\examples{

## The function is currently defined as
function (genes.aliases) 
{
    require(org.Hs.eg.db)
    all.aliases <- toTable(org.Hs.egALIAS2EG)
    gene.symbols <- toTable(org.Hs.egSYMBOL)
    rownames(gene.symbols) <- gene.symbols[, 1]
    names(genes.aliases) <- genes.aliases
    alias.inds <- which(!(genes.aliases \%in\% gene.symbols[, 2]))
    inds <- which(toupper(all.aliases[, 2]) \%in\% toupper(genes.aliases[alias.inds]))
    entrez.ids <- all.aliases[inds, 1]
    names(entrez.ids) <- all.aliases[inds, 2]
    genes.aliases[names(entrez.ids)] <- gene.symbols[entrez.ids, 
        2]
    return(genes.aliases)
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
