\name{lasso.regression}
\alias{lasso.regression}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Lasso regression as constrained least squares
}
\description{
Lasso regression implemented as a constrained least squares problem. Allows for additional constraints to be added to the model. This function uses the limSolve package. 
}
\usage{
lasso.regression(y, feature.matrix, holdout.set = c(), lambda = seq(0.5, 5, 0.25), standardize = TRUE, exclude = c())
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{y}{
%%     ~~Describe \code{y} here~~
}
  \item{feature.matrix}{
%%     ~~Describe \code{feature.matrix} here~~
}
  \item{holdout.set}{
%%     ~~Describe \code{holdout.set} here~~
}
  \item{lambda}{
%%     ~~Describe \code{lambda} here~~
}
  \item{standardize}{
%%     ~~Describe \code{standardize} here~~
}
  \item{exclude}{
%%     ~~Describe \code{exclude} here~~
}
  \item{sign.constraints}{
Named numeric vector. 1 represents the feature should be positive, -1 indicates feature should be negative.
}
  \item{penalty.factor}{
}
}
\details{

\deqn{p(x) = \frac{\lambda^x e^{-\lambda}}{x!}}{%
p(x) = lambda^x exp(-lambda)/x!}
for \eqn{x = 0, 1, 2, \ldots}.

The constraints are represented as: \deqn{w = u - v; u,v >= 0}
\deqn{\sum(u,v) <= lambda}

}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (y, feature.matrix, holdout.set = c(), lambda = seq(0.5, 
    5, 0.25), standardize = TRUE, exclude = c()) 
{
    require(limSolve)
    if (standardize) 
        feature.matrix <- normalize(feature.matrix, 2)
    if (hasArg(exclude)) 
        feature.matrix <- feature.matrix[, -exclude]
    if (hasArg(holdout.set)) {
        prediction.features <- feature.matrix[holdout.set, ]
        feature.matrix <- feature.matrix[-holdout.set, ]
        y <- y[-holdout.set]
    }
    else {
        prediction.features <- feature.matrix
    }
    A <- cbind(feature.matrix, -feature.matrix)
    b <- y
    G <- diag(1, ncol(A))
    h <- rep(0, ncol(A))
    G <- rbind(G, rep(-1, ncol(G)))
    h <- c(h, 0)
    coef <- matrix(0, ncol(feature.matrix), length(lambda))
    for (iter in 1:length(lambda)) {
        h[length(h)] <- -lambda[iter]
        sol <- lsei(A = A, B = b, G = G, H = h)
        coef.matrix <- matrix(sol$X, ncol = 2)
        coef[, iter] <- coef.matrix[, 1] - coef.matrix[, 2]
    }
    ypre <- prediction.features \%*\% coef
    rm(sol)
    gc()
    return(list(ypre = ypre, coef = coef, lambda = lambda))
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
